const path = require('path');
const ExtractCss = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const UglifyPlugin = require('uglifyjs-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const OptimizeCssPlugin = require('optimize-css-assets-webpack-plugin');

module.exports = {
    entry: path.join(__dirname, 'src', 'index.js'),
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist')
    },
    optimization: {
        minimizer: [
            new TerserPlugin({}),
            new OptimizeCssPlugin({}),
            new UglifyPlugin()
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: 'Tajam Land',
            filename: 'index.html',
            template: path.join(__dirname, 'src', 'index.html')
        }),
        new ExtractCss({
            filename: 'index.css'
        }),
    ],
    resolve: {
        extensions: ['.js', '.ts']
    },
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        port: 3000
    },
    module: {
        rules: [
            {
                test: /.html$/,
                use: ['html-loader']
            },
            {
                test: /\.(png|jpe?g|gif)$/,
                use: [
                  {
                    loader: 'file-loader',
                    options: {
                        name: '[path][name].[ext]',
                    },
                  },
                ],
            },
            {
                test: /.scss$/,
                use: [
                    ExtractCss.loader,
                    'css-loader',
                    'sass-loader'
                ]
            },
            {
                test: /(.js|.ts)$/,
                exclude: /(node_modules|bower_components)/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            presets: ['@babel/preset-env', '@babel/preset-typescript']
                        }
                    }
                ]
            }
        ]
    }
}